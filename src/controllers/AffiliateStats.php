<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use SilverStripe\Security\Security;

class AffiliateStats extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
    );

    public function init() {
        parent::init();

        if (!Security::getCurrentUser()){
            return $this->httpError(404);
        }

    }

    public function TotalToday(){

        $today = new \DateTime();

        $sales = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $today->format('Y-m-d 00:00:00')));

        $commission = 0;
        foreach ($sales as $node) {

            $commission = $commission + $node->Commission;

        }

        return number_format($commission, 2, ',', '');

    }

    public function TotalYesterday(){

        $today = new \DateTime();
        $today->modify('yesterday');

        $sales = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $today->format('Y-m-d 00:00:00'), 'SaleDate:LessThanOrEqual' => $today->format('Y-m-d 23:59:59')));

        $commission = 0;
        foreach ($sales as $node) {

            $commission = $commission + $node->Commission;

        }

        return number_format($commission, 2, ',', '');

    }

    public function CurrentMonthStats(){

        $start = new \DateTime();
        $start->modify('first day of this month');

        $end = new \DateTime();
        $end->modify('first day of next month');

        return new ArrayData($this->MonthStats($start->format('Y-m-d 00:00:00'), $end->format('Y-m-d 00:00:00')));

    }

    public function MonthStats($start, $end){

        $month = new \DateTime($start);

        $current = new \DateTime();
        $monthdays = new \DateTime();
        $monthdays->modify('last day of this month');

        $sales = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $start, 'SaleDate:LessThan' => $end));
        $salesGoogle = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $start, 'SaleDate:LessThan' => $end, 'Status' => 'approved', 'Visitor.Source' => 'GoogleAds'));
        $salesBing = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $start, 'SaleDate:LessThan' => $end, 'Status' => 'approved', 'Visitor.Source' => 'BingAds'));
        //$sales = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $start, 'SaleDate:LessThan' => $end));

        $open = 0;
        $approved = 0;
        $bruto = 0;
        $disapproved = 0;
        foreach ($sales as $node) {

            $bruto = $bruto + $node->Commission;

            if ($node->Status === 'approved'){

                $approved = $approved + $node->Commission;

            }
            if ($node->Status === 'open'){

                $open = $open + $node->Commission;

            }
            if ($node->Status === 'disapproved'){

                $disapproved = $disapproved + $node->Commission;

            }

        }

        $approvedGoogle = 0;
        foreach ($salesGoogle as $node) {

            $approvedGoogle = $approvedGoogle + $node->Commission;

        }

        $approvedBing = 0;
        foreach ($salesBing as $node) {

            $approvedBing = $approvedBing + $node->Commission;

        }

        $adscosts = AdsCost::get()->filter('MonthDate', $month->format('Y-m-t'))->first();
        $adscosts_googleads = 0;
        if (isset($adscosts->GoogleAds) && $adscosts->GoogleAds > 0){
            $adscosts_googleads = $adscosts->GoogleAds;
        }
        $adscosts_bingads = 0;
        if (isset($adscosts->BingAds) && $adscosts->BingAds > 0){
            $adscosts_bingads = $adscosts->BingAds;
        }

        $brutoprognosis = ($bruto / $current->format('d')) * $monthdays->format('d');
        $approvedprognosiscurrent = $brutoprognosis * $this->getApprovedPercent();
        $approvedprognosis = $bruto * $this->getApprovedPercent();
        $googlenetto = $approvedGoogle - $adscosts_googleads;
        $googlenettocolor = "green";
        if ($googlenetto < 10){
            $googlenettocolor = "orange";
        }
        if ($googlenetto < 0){
            $googlenettocolor = "red";
        }
        $bingnetto = $approvedBing - $adscosts_bingads;
        $bingnettocolor = "green";
        if ($bingnetto < 10){
            $bingnettocolor = "orange";
        }
        if ($bingnetto < 0){
            $bingnettocolor = "red";
        }
        $percent = 0;
        if ($approved > 0){
            $percent = round(($approved / ($disapproved + $approved)) * 100);
        }

        $output =  array(
            'Month' => $month->format('F Y'),
            'Bruto' => number_format($bruto, 2, ',', ''),
            'Approved' => number_format($approved, 2, ',', ''),
            'Open' => number_format($open, 2, ',', ''),
            'Percent' => $percent,
            'Disapproved' => number_format($disapproved, 2, ',', ''),
            'BrutoPerDay' => number_format($bruto / $current->format('d'), 2, ',', ''),
            'BrutoPrognosis' => number_format($brutoprognosis, 2, ',', ''),
            'ApprovedPrognosis' => number_format($approvedprognosis, 2, ',', ''),
            'ApprovedPrognosisCurrent' => number_format($approvedprognosiscurrent, 2, ',', ''),
            'AverageApprovedPercent' => round(($this->getApprovedPercent() * 100)),
            'ApprovedGoogle' => number_format($approvedGoogle, 2, ',', ''),
            'SalesGoogle' => $salesGoogle->Count(),
            'ApprovedBing' => number_format($approvedBing, 2, ',', ''),
            'SalesBing' => $salesBing->Count(),
            'GoogleCosts' => number_format($adscosts_googleads, 2, ',', ''),
            'GoogleNetto' => number_format($googlenetto, 2, ',', ''),
            'GoogleNettoColor' => $googlenettocolor,
            'BingCosts' => number_format($adscosts_bingads, 2, ',', ''),
            'BingNetto' => number_format($bingnetto, 2, ',', ''),
            'BingNettoColor' => $bingnettocolor
        );

        return $output;

    }

    public function Months(){

        $current = new \DateTime();
        $current->modify('first day of previous month');

        $monthlist = new ArrayList();
        $count = 0;
        while ($count < 12) {

            $start = new \DateTime($current->format('Y-m-d'));
            $start->modify('- '.$count.' months');

            $end = new \DateTime($start->format('Y-m-d'));
            $end->modify('+ 1 month');

            $monthlist->push(
                new ArrayData($this->MonthStats($start->format('Y-m-d 00:00:00'), $end->format('Y-m-d 00:00:00')))
            );

            $count++;

        }

        return $monthlist;

    }

    public function getApprovedPercent(){

        $start = new \DateTime();
        $start->modify('first day of 6 months ago');

        $end = new \DateTime();
        $end->modify('first day of 3 months ago');

        $sales = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $start->format('Y-m-d 00:00:00'), 'SaleDate:LessThan' => $end->format('Y-m-d 00:00:00')));

        $approved = 0;
        $disapproved = 0;
        foreach ($sales as $node) {

            if ($node->Status === 'approved'){

                $approved = $approved + $node->Commission;

            }
            if ($node->Status === 'disapproved'){

                $disapproved = $disapproved + $node->Commission;

            }

        }

        if ($approved == 0 && $disapproved == 0){
            return 0;
        }

        return round($approved / ($disapproved + $approved), 2);

    }

    public function Totals($period = "last12months"){

        $start = new \DateTime();
        $end = new \DateTime();
        if ($period == 'currentyear') {
            $start->modify('first day of January this year');
            $end->modify('first day next month');
        }elseif ($period == 'lastyear'){
            $start->modify('first day of January last year');
            $end->modify('first day of January this year');
        }else{
            $start->modify('first day of 11 months ago');
            $end->modify('first day next month');
        }

        $sales = Sale::get()->filter(array('SaleDate:GreaterThanOrEqual' => $start->format('Y-m-d 00:00:00'), 'SaleDate:LessThan' => $end->format('Y-m-d 00:00:00'), 'Status' => 'approved'));

        $approved = 0;
        foreach ($sales as $node) {

            $approved = $approved + $node->Commission;

        }

        $adscosts = AdsCost::get()->filter(array('MonthDate:GreaterThanOrEqual' => $start->format('Y-m-d 00:00:00'), 'MonthDate:LessThan' => $end->format('Y-m-d 00:00:00')));

        $adsspent = 0;
        foreach ($adscosts as $node) {

            $adsspent = $adsspent + $node->GoogleAds + $node->BingAds;

        }

        $netto = $approved - $adsspent;

        $salescount = 1;
        if ($sales->count() > 0){
            $salescount = $sales->count();
        }

        $output =  array(
            'Approved' => number_format($approved, 2, ',', ''),
            'SalesCount' => $sales->count(),
            'AverageSale' => number_format($approved / $salescount, 2, ',', ''),
            'AdsCosts' => number_format($adsspent, 2, ',', ''),
            'Netto' => number_format($netto, 2, ',', ''),
        );

        return new ArrayData($output);

    }

}
