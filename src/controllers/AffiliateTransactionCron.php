<?php

use SilverStripe\Control\Email\Email;
use Hestec\LinkManager\Sale;
use Hestec\LinkManager\Visitor;
use Hestec\LinkManager\CronLog;
use Hestec\LinkManager\AwinProgram;
use SilverStripe\Core\Config\Config;
use SilverStripe\CMS\Model\SiteTree;
use Hestec\LinkManager\Widget;
use Hestec\LinkManager\Click;
use Hestec\LinkManager\DeletedSale;
use SilverStripe\ORM\DB;
use Hestec\GoogleBingAdsApi\BingConversion;
use Hestec\GoogleBingAdsApi\GoogleConversion;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use Hestec\LinkManager\ApiToken;

class AffiliateTransactionCron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'Sales',
        'Clicks',
        'DbCleanup',
        'DaisyconAuthorize',
        'DaisyconAccessToken',
        'test'
    );

    public function ytest(){

        //$saletime = new DateTime('2020-12-09 20:00:00');
        //$saletime->getTimestamp();

        $conversion = new BingConversion();
        $conversion->setConversionName('AffiliateSale');
        $conversion->setConversionTime('2021-09-24 14:20:00');
        $conversion->setConversionValue(1);
        $conversion->setConversionClickId('439fdbb0a4ef17d4e156bafd362f2158'); // visitor 500757
        //$conversion->setConversionClickId('f894f652ea334e739002f7167ab8f8e3');
        $output = $conversion->add();

        return $output;

    }

    public function abctest(){

        //$saletime = new DateTime('2020-12-09 20:00:00');
        //$saletime->getTimestamp();

        /*$conversion = new GoogleConversion();
        //$conversion->setConversionName('AffiliateSale1');
        $conversion->setConversionTime('2022-06-21 10:00:00');
        $conversion->setConversionValue(1);
        $conversion->setConversionClickId('CjwKCAjwtcCVBhA0EiwAT1fY72-ZPBtRKKYGLZJi0EjT1h--hE6Jeowq_sa84HGzfIYdwuVR2Tyr6RoC9KcQAvD_BwE'); // visitor 765108
        //$conversion->setConversionClickId('f894f652ea334e739002f7167ab8f8e3');
        $output = $conversion->add();*/

        $clickid = '0603a46c75f01480a84ac6ddc34588a3'; // BING
        //$clickid = 'CjwKCAjwtcCVBhA0EiwAT1fY72-ZPBtRKKYGLZJi0EjT1h--hE6Jeowq_sa84HGzfIYdwuVR2Tyr6RoC9KcQAvD_BwE'; // GOOGLE visitor 765108
        $saledate = '2022-06-22 20:02:00';
        $value = 1;

        //try{
        $output = $this->addConversion('BingAds', $clickid, $saledate, $value);
        //} catch (\Exception $e){
        //echo $e->getMessage();
        //}

        if ($output == true){
            return "doorgegaan met true";
        }
        return "doorgegaan met false";

    }

    public function xxtest(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $affdetails = Config::inst()->get('AffiliateDetails');

            $pz_publisher_id = $affdetails['pz_publisher_id'];
            $pz_user_api_key = $affdetails['pz_user_api_key'];
            $pz_api_key = $affdetails['pz_api_key'];
            $affiliatesite = $affdetails['AffiliateSite'];

            $start = new \DateTime();
            $start->modify('- 7 days');

            $end = new \DateTime();
            $end->modify('tomorrow');

            /*$client = new TradeTrackerClient($tt_publisher_id, $tt_api_key);
            //$transactions = $client->getTransactions($start->format('Y-m-d'), $end->format('Y-m-d'));
            $transactions = $client->getTransactions(new \DateTime($start->format('Y-m-d')), new \DateTime());

            print_r($transactions);*/

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://api.partnerize.com/reporting/report_publisher/publisher/'.$pz_publisher_id.'/conversion.json?start_date='.$start->format('Y-m-d H:i:s').'&date_type=last_updated ', [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ],
                    'auth' => [
                        $pz_user_api_key,
                        $pz_api_key
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Daisycon Sales cron", $e->getMessage());
                $email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                foreach ($msgs->conversions as $node) {

                    echo $node->conversion_data->publisher_reference."<br>";

                }

                //return print_r($msgs);

            }

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    function GenerateCodeVerifier($n)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-._~';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function hash($code)
    {
        return str_replace(
            '=',
            '',
            strtr(
                base64_encode(
                    hash('sha256', $code, true)
                ),
                '+/',
                '-_'
            )
        );
    }

    public function DaisyconAuthorize(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $affdetails = Config::inst()->get('AffiliateDetails');
            $dc_client_id = $affdetails['dc_client_id'];
            $dc_client_secret = $affdetails['dc_client_secret'];

            if (isset($_GET['code'])) {

                $data = [
                    'grant_type' => 'authorization_code',
                    'redirect_uri' => 'https://' . $_SERVER['HTTP_HOST'] . '/AffiliateTransactionCron/DaisyconAuthorize',
                    'client_id' => $dc_client_id,
                    'client_secret' => $dc_client_secret,
                    'code' => $_GET['code'],
                    'code_verifier' => $this->getRequest()->getSession()->get('codeverifier')
                ];
                $this->getRequest()->getSession()->clear('codeverifier');

                $client = new \GuzzleHttp\Client();
                try {
                    $response = $client->request('POST', 'https://login.daisycon.com/oauth/access-token', [
                        'curl' => [
                            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                        ],
                        'form_params' => $data
                    ]);

                } catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    $error = true;

                    //throw new Exception($e->getResponse()->getBody()->getContents());

                }
                if (!isset($error)) {

                    $msgs = json_decode($response->getBody());

                    $exp_time = new \DateTime();
                    $exp_time->modify('+'.$msgs->expires_in.' seconds');

                    $at = ApiToken::get()->first();
                    $at->DcAccessToken = $msgs->access_token;
                    $at->DcAccessTokenExpire = $exp_time->format('Y-m-d H:i:s');;
                    $at->DcRefreshToken = $msgs->refresh_token;
                    $at->write();

                    return "success stored access and refresh";

                }

            } else {

                $code_verifier = $this->GenerateCodeVerifier(64);
                $codeverifier_hash = $this->hash($code_verifier);
                $this->getRequest()->getSession()->set('codeverifier', $code_verifier);

                return $this->redirect('https://login.daisycon.com/oauth/authorize?response_type=code&client_id=' . $dc_client_id . '&redirect_uri=https://' . $_SERVER['HTTP_HOST'] . '/AffiliateTransactionCron/DaisyconAuthorize&code_challenge=' . $codeverifier_hash);

            }
            return "ergens ging iet mis";

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function DaisyconAccessToken(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $now = new \DateTime();

            $affdetails = Config::inst()->get('AffiliateDetails');
            $dc_client_id = $affdetails['dc_client_id'];
            $dc_client_secret = $affdetails['dc_client_secret'];

            $at = ApiToken::get()->first();
            $exp = new \DateTime($at->DcAccessTokenExpire);
            $exp->modify('-1 minute');
            if ($exp > $now){

                return $at->DcAccessToken;

            }

            $dc_refresh_token = $at->DcRefreshToken;

            $data = [
                'grant_type' => 'refresh_token',
                'redirect_uri' => 'https://' . $_SERVER['HTTP_HOST'] . '/AffiliateTransactionCron/DaisyconAuthorize',
                'client_id' => $dc_client_id,
                'client_secret' => $dc_client_secret,
                'refresh_token' => $dc_refresh_token
            ];
            $this->getRequest()->getSession()->clear('codeverifier');

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('POST', 'https://login.daisycon.com/oauth/access-token', [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ],
                    'form_params' => $data
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Daisycon Accesstoken", $e->getMessage());
                $email->sendPlain();
                $error = true;

                //throw new Exception($e->getResponse()->getBody()->getContents());

            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                $exp_time = new \DateTime();
                $exp_time->modify('+'.$msgs->expires_in.' seconds');

                $at->DcAccessToken = $msgs->access_token;
                $at->DcAccessTokenExpire = $exp_time->format('Y-m-d H:i:s');;
                $at->DcRefreshToken = $msgs->refresh_token;
                $at->write();

                return $msgs->access_token;

            }

            return false;

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $affdetails = Config::inst()->get('AffiliateDetails');

            $dc_publisher_id = $affdetails['dc_publisher_id'];
            $dc_media_id = $affdetails['dc_media_id'];

            $lastrun = CronLog::get()->first();
            $end = new \DateTime();
            $start = new \DateTime($lastrun->LastRun);
            $start->modify('-24 hours');
            //$start->modify('-10 days');

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://services.daisycon.com/publishers/'.$dc_publisher_id.'/transactions?media_id='.$dc_media_id.'&date_modified_start='.$start->format('Y-m-d H:i:s').'&date_modified_end='.$end->format('Y-m-d'), [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ],
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->DaisyconAccessToken(),
                        'Accept'        => 'application/json'
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Daisycon Sales cron", $e->getMessage());
                $email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                foreach ($msgs as $node) {

                    echo $node->affiliatemarketing_id."<br>";

                }

                //return print_r($msgs);

            }

            //return $this->hash($apitoken->DcCodeVerifier);

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function yyytest(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $affdetails = Config::inst()->get('AffiliateDetails');

            $tt_media_id = $affdetails['tt_media_id'];
            $tt_publisher_id = $affdetails['tt_publisher_id'];
            $tt_api_key = $affdetails['tt_api_key'];
            $affiliatesite = $affdetails['AffiliateSite'];

            $start = new \DateTime();
            $start->modify('yesterday');

            $end = new \DateTime();
            $end->modify('tomorrow');

            /*$client = new TradeTrackerClient($tt_publisher_id, $tt_api_key);
            //$transactions = $client->getTransactions($start->format('Y-m-d'), $end->format('Y-m-d'));
            $transactions = $client->getTransactions(new \DateTime($start->format('Y-m-d')), new \DateTime());

            print_r($transactions);*/

            $client = new SoapClient('http://ws.tradetracker.com/soap/affiliate?wsdl', array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));
            $client->authenticate($tt_publisher_id, $tt_api_key);

            $affiliateSiteID = $tt_media_id;

            $options = array (
                'registrationDateFrom' => $start->format('Y-m-d'),
                'transactionType' => "sale"
            );

            foreach ($client->getConversionTransactions($affiliateSiteID, $options) as $transaction) {
                echo $transaction->registrationDate . ' - ' . $transaction->campaign->name . ' - ' . $transaction->commission . ' - ' . $transaction->reference . ' - ' . $transaction->ID . ' - ' . $transaction->IP . '<br />';
            }

            print_r($client->getConversionTransactions($affiliateSiteID, $options));

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function Sales() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $affdetails = Config::inst()->get('AffiliateDetails');

            if (isset($affdetails['UseMobile0318']) && $affdetails['UseMobile0318'] == true){
                $this->Mobile0318Clicks();
            }
            if (isset($affdetails['UseDaisycon']) && $affdetails['UseDaisycon'] == true){
                $this->DaisyconSales();
            }
            if (isset($affdetails['UseAwin']) && $affdetails['UseAwin'] == true){
                $this->AwinSales('transaction');
            }
            if (isset($affdetails['UseAwin']) && $affdetails['UseAwin'] == true){
                $this->AwinSales('validation');
            }
            if (isset($affdetails['UseTradetracker']) && $affdetails['UseTradetracker'] == true){
                $this->TradetrackerSales('transaction');
            }
            if (isset($affdetails['UseTradetracker']) && $affdetails['UseTradetracker'] == true){
                $this->TradetrackerSales('validation');
            }
            if (isset($affdetails['UsePartnerize']) && $affdetails['UsePartnerize'] == true){
                $this->PartnerizeSales();
            }

            $runtime = new \DateTime();
            $log = CronLog::get()->first();
            $log->LastRun = $runtime->format('Y-m-d H:i:s');
            $log->write();

            // get once a day an update of the Awin programs
            if (isset($affdetails['UseAwin']) && $affdetails['UseAwin'] == true && $runtime->format('H') == '00'){
                $this->AwinProgram();
            }

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function Clicks() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $this->Mobile0318Clicks();

            /*$runtime = new \DateTime();
            $log = CronLog::get()->first();
            $log->LastRun = $runtime->format('Y-m-d H:i:s');
            $log->write();*/

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function DaisyconSales() {

        $affdetails = Config::inst()->get('AffiliateDetails');

        $dc_publisher_id = $affdetails['dc_publisher_id'];
        $dc_media_id = $affdetails['dc_media_id'];
        $affiliatesite = $affdetails['AffiliateSite'];

        $lastrun = CronLog::get()->first();
        $end = new \DateTime();
        $start = new \DateTime($lastrun->LastRun);
        $start->modify('-24 hours');
        //$start->modify('-10 days');

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'https://services.daisycon.com/publishers/'.$dc_publisher_id.'/transactions?media_id='.$dc_media_id.'&date_modified_start='.$start->format('Y-m-d H:i:s').'&date_modified_end='.$end->format('Y-m-d'), [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->DaisyconAccessToken(),
                    'Accept'        => 'application/json'
                ]
            ]);

        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Daisycon Sales cron", $e->getMessage());
            $email->sendPlain();
            $error = true;
        }
        if (!isset($error)) {

            $msgs = json_decode($response->getBody());

            if (!empty($msgs)) {

                foreach ($msgs as $node) {

                    $noVisitorId = false;

                    $product = '';
                    $commission = 0;
                    $disapprovedcommission = 0;
                    $disapprovedreason = '';
                    $approvaldate = '';
                    $arr_status = array();
                    foreach ($node->parts as $part) {

                        $arr_status[] = $part->status;

                        $ClickDate = $part->date_click;
                        $SubId1 = $part->subid;
                        $SubId2 = $part->subid_2;
                        $SubId3 = $part->subid_3;

                        $product .= $part->publisher_description." | ".$part->commission." | ".$part->status."\n";

                        if ($part->status != 'disapproved'){

                            $commission = $commission + $part->commission;

                        }else{

                            $disapprovedcommission = $disapprovedcommission + $part->commission;
                            $disapprovedreason = $part->disapproved_reason;

                        }

                        if ($part->status != 'open'){

                            $approvaldate = $part->approval_date;

                        }

                    }

                    $status = '';
                    if (in_array('disapproved', $arr_status)){
                        $status = 'disapproved';
                    }
                    if (in_array('approved', $arr_status)){
                        $status = 'approved';
                        $disapprovedreason = '';
                    }
                    if (in_array('open', $arr_status)){
                        $status = 'open';
                        $disapprovedreason = '';
                        $approvaldate = '';
                    }

                    $siteid = false;
                    $visitorid = false;
                    $clickid = false;
                    $widgetid = false;
                    $pageid = false;

                    $clickparams = $this->getClickParams($SubId1.$SubId2.$SubId3);

                    if (isset($clickparams[0]) && is_numeric($clickparams[0])){
                        $siteid = $clickparams[0];
                    }
                    if (isset($clickparams[1]) && is_numeric($clickparams[1])){
                        $visitorid = $clickparams[1];
                    }
                    if (isset($clickparams[2]) && is_numeric($clickparams[2])){
                        $clickid = $clickparams[2];
                    }
                    if (isset($clickparams[3]) && is_numeric($clickparams[3])){
                        $widgetid = $clickparams[3];
                    }
                    if (isset($clickparams[4]) && is_numeric($clickparams[4])){
                        $pageid = $clickparams[4];
                    }

                    if (!$visitorid && !$clickid && strlen($SubId1.$SubId2.$SubId3) > 30 && isset($affdetails['UseMobile0318']) && $affdetails['UseMobile0318'] == true) {

                        if ($click = $this->getMobile0318Sale($SubId1.$SubId2.$SubId3)) {

                            $siteid = $affiliatesite;
                            $visitorid = $click->VisitorID;
                            $clickid = $click->ID;

                        }

                    }

                    $dc_carinsuranceprograms = [68,317,13184]; // unitedcosumers, ohra, nn

                    if (!$visitorid && !$clickid && strlen($SubId1.$SubId2.$SubId3) > 20 && isset($affdetails['UseOverstappen']) && $affdetails['UseOverstappen'] == true && in_array($node->program_id, $dc_carinsuranceprograms)) {

                        if ($click = $this->getOverstappenClick($ClickDate)) {

                            $siteid = $affiliatesite;
                            $visitorid = $click->VisitorID;
                            $widgetid = 5;
                            $pageid = 8; // autoverzekering pagina kz

                        }

                    }

                    if ($siteid == $affiliatesite || $siteid == false) {

                        $addconversion = false;

                        if (!$sale = Sale::get()->filter(array('TransactionId' => $node->affiliatemarketing_id, 'AffiliateNetwork' => "Daisycon"))->first()) {

                            $sale = new Sale();

                            if ($visitorid && $visitordata = Visitor::get()->byID($visitorid)) {

                                $sale->VisitorID = $visitorid;
                                $sale->TrackType = 'Normal';

                                if (($visitordata->Source == "BingAds" || $visitordata->Source == "GoogleAds") && $visitordata->SourceClickId){

                                    //$addconversion = true;

                                }

                                if (!is_numeric($clickid) && $widgetid > 0 && $pageid > 0) {

                                    $widgettitle = "unknown widget";
                                    if ($widget = Widget::get()->byID($widgetid)){
                                        $widgettitle = $widget->Title;
                                    }

                                    $click = new Click();
                                    if ($page = SiteTree::get()->byID($pageid)) {
                                        $click->RefPage = $page->Link();
                                    }
                                    $click->ClickDate = $ClickDate;
                                    $click->Action = "Click from widget: ".$widgettitle;
                                    $click->VisitorID = $visitorid;
                                    $click->write();

                                    $sale->ClickID = $click->ID;

                                }else{

                                    $sale->ClickID = $clickid;

                                }

                            } else {

                                $noVisitorId = true;
                                $sale->TrackType = 'UnknownVisitor';

                            }

                            $sale->TransactionId = $node->affiliatemarketing_id;
                            $sale->AffiliateNetwork = "Daisycon";

                        }

                        $sale->SaleDate = $node->date;
                        $sale->DeviceType = $node->device_type;
                        $sale->ProgramId = $node->program_id;
                        $sale->ProgramName = $node->program_name;
                        $sale->Product = $product;
                        $sale->Status = $status;
                        $sale->ApprovalDate = $approvaldate;
                        $sale->DisapprovedReason = $disapprovedreason;
                        $sale->Commission = $commission;
                        if ($status == 'disapproved'){
                            $sale->Commission = $disapprovedcommission;
                        }else{
                            $sale->Commission = $commission;
                        }

                        $sale->ClickDate = $ClickDate;
                        $sale->SubId1 = $SubId1;
                        $sale->SubId2 = $SubId2;
                        $sale->SubId3 = $SubId3;

                        $sale->write();

                        if ($addconversion == true){
                            $this->addConversion($visitordata->Source, $visitordata->SourceClickId, $node->date, $commission);
                        }

                        if ($noVisitorId == true) {

                            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Sale kon niet aan visitor worden gekoppeld", "SaleID: " . $sale->ID);
                            $email->sendPlain();

                        }

                    }

                }

            }

        }

    }

    public function PartnerizeSales() {

        $affdetails = Config::inst()->get('AffiliateDetails');

        $pz_publisher_id = $affdetails['pz_publisher_id'];
        $pz_user_api_key = $affdetails['pz_user_api_key'];
        $pz_api_key = $affdetails['pz_api_key'];
        $affiliatesite = $affdetails['AffiliateSite'];

        $lastrun = CronLog::get()->first();
        //$end = new \DateTime();
        $start = new \DateTime($lastrun->LastRun);
        $start->modify('-24 hours');
        //$start->modify('-7 days');

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'https://api.partnerize.com/reporting/report_publisher/publisher/'.$pz_publisher_id.'/conversion.json?start_date='.$start->format('Y-m-d H:i:s').'&timezone=Europe/Amsterdam&date_type=last_updated ', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ],
                'auth' => [
                    $pz_user_api_key,
                    $pz_api_key
                ]
            ]);

        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Partnerize Sales cron", $e->getMessage());
            $email->sendPlain();
            $error = true;
        }
        if (!isset($error)) {

            $msgs = json_decode($response->getBody());

            if (!empty($msgs)) {

                foreach ($msgs->conversions as $node) {

                    $noVisitorId = false;

                    $product = '';
                    $disapprovedreason = '';
                    $approvaldate = '';

                    foreach ($node->conversion_data->conversion_items as $part) {

                        $product .= $part->category." | ".$part->sku." | ".$part->item_publisher_commission." | ".$part->item_status."\n";

                        if ($part->item_status == 'rejected'){

                            $disapprovedreason .= $part->reject_reason." ";

                        }

                        if ($part->item_status != 'pending'){

                            $approvaldate = $part->approved_at;

                        }

                    }


                    $status = '';
                    if ($node->conversion_data->conversion_value->conversion_status == "rejected"){
                        $status = 'disapproved';
                    }
                    if ($node->conversion_data->conversion_value->conversion_status == "approved"){
                        $status = 'approved';
                    }
                    if ($node->conversion_data->conversion_value->conversion_status == "pending"){
                        $status = 'open';
                    }

                    $siteid = false;
                    $visitorid = false;
                    $clickid = false;
                    $widgetid = false;
                    $pageid = false;

                    $clickparams = $this->getClickParams(str_replace('-', '_', $node->conversion_data->publisher_reference));

                    if (isset($clickparams[0]) && is_numeric($clickparams[0])){
                        $siteid = $clickparams[0];
                    }
                    if (isset($clickparams[1]) && is_numeric($clickparams[1])){
                        $visitorid = $clickparams[1];
                    }
                    if (isset($clickparams[2]) && is_numeric($clickparams[2])){
                        $clickid = $clickparams[2];
                    }
                    if (isset($clickparams[3]) && is_numeric($clickparams[3])){
                        $widgetid = $clickparams[3];
                    }
                    if (isset($clickparams[4]) && is_numeric($clickparams[4])){
                        $pageid = $clickparams[4];
                    }

                    if ($siteid == $affiliatesite || $siteid == false) {

                        $addconversion = false;

                        if (!$sale = Sale::get()->filter(array('TransactionId' => $node->conversion_data->conversion_id, 'AffiliateNetwork' => "Partnerize"))->first()) {

                            $sale = new Sale();

                            if ($visitorid && $visitordata = Visitor::get()->byID($visitorid)) {

                                $sale->VisitorID = $visitorid;
                                $sale->TrackType = 'Normal';

                                if (($visitordata->Source == "BingAds" || $visitordata->Source == "GoogleAds") && $visitordata->SourceClickId){

                                    //$addconversion = true;

                                }

                                if (!is_numeric($clickid) && $widgetid > 0 && $pageid > 0) {

                                    $widgettitle = "unknown widget";
                                    if ($widget = Widget::get()->byID($widgetid)){
                                        $widgettitle = $widget->Title;
                                    }

                                    $click = new Click();
                                    if ($page = SiteTree::get()->byID($pageid)) {
                                        $click->RefPage = $page->Link();
                                    }
                                    $click->ClickDate = $node->conversion_data->click->set_time;
                                    $click->Action = "Click from widget: ".$widgettitle;
                                    $click->VisitorID = $visitorid;
                                    $click->write();

                                    $sale->ClickID = $click->ID;

                                }else{

                                    $sale->ClickID = $clickid;

                                }

                            } else {

                                $noVisitorId = true;
                                $sale->TrackType = 'UnknownVisitor';

                            }

                            $sale->TransactionId = $node->conversion_data->conversion_id;
                            $sale->AffiliateNetwork = "Partnerize";

                        }

                        $sale->SaleDate = $node->conversion_data->conversion_time;
                        //$sale->DeviceType = $node->device_type;
                        $sale->ProgramId = $node->conversion_data->campaign_id;
                        $sale->ProgramName = $node->conversion_data->campaign_title;
                        $sale->Product = $product;
                        $sale->Status = $status;
                        $sale->ApprovalDate = $approvaldate;
                        $sale->DisapprovedReason = $disapprovedreason;
                        $sale->Commission = $node->conversion_data->conversion_value->publisher_commission;

                        $sale->ClickDate = $node->conversion_data->click->set_time;
                        $sale->SubId1 = $node->conversion_data->publisher_reference;

                        $sale->write();

                        if ($addconversion == true){
                            $this->addConversion($visitordata->Source, $visitordata->SourceClickId, $node->conversion_data->conversion_time, $node->conversion_data->conversion_value->publisher_commission);
                        }

                        if ($noVisitorId == true) {

                            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Sale kon niet aan visitor worden gekoppeld", "SaleID: " . $sale->ID);
                            $email->sendPlain();

                        }

                    }

                }

            }

        }

    }

    public function TradetrackerSales($datetype = 'transaction') {

        $affdetails = Config::inst()->get('AffiliateDetails');

        $tt_media_id = $affdetails['tt_media_id'];
        $tt_publisher_id = $affdetails['tt_publisher_id'];
        $tt_api_key = $affdetails['tt_api_key'];
        $affiliatesite = $affdetails['AffiliateSite'];

        $lastrun = CronLog::get()->first();
        $start = new \DateTime($lastrun->LastRun);
        $start->modify('-24 hours');


        $file_headers = @get_headers('https://ws.tradetracker.com/soap/affiliate?wsdl');
        if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Tradetracker Sales cron", "URL check returns: HTTP/1.1 404 Not Found");
            $email->sendPlain();
            $error = true;
        } else {
            $client = new SoapClient('https://ws.tradetracker.com/soap/affiliate?wsdl', array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));
            $client->authenticate($tt_publisher_id, $tt_api_key);

            $affiliateSiteID = $tt_media_id;

            $options = array(
                'registrationDateFrom' => $start->format('Y-m-d'),
                'transactionType' => "sale"
            );

            if ($datetype == 'validation') {
                $options = array(
                    'assessmentDateFrom' => $start->format('Y-m-d'),
                    'transactionType' => "sale"
                );
            }
        }

        if (!isset($error)) {

            $msgs = $client->getConversionTransactions($affiliateSiteID, $options);

            if (!empty($msgs)) {

                foreach ($msgs as $node) {

                    $noVisitorId = false;

                    $status = '';
                    if ($node->transactionStatus == "rejected"){
                        $status = 'disapproved';
                    }
                    if ($node->transactionStatus == "accepted"){
                        $status = 'approved';
                    }
                    if ($node->transactionStatus == "pending"){
                        $status = 'open';
                    }

                    $siteid = false;
                    $visitorid = false;
                    $clickid = false;
                    $widgetid = false;
                    $pageid = false;

                    $clickparams = $this->getClickParams(str_replace('-', '_', $node->reference));

                    if (isset($clickparams[0]) && is_numeric($clickparams[0])){
                        $siteid = $clickparams[0];
                    }
                    if (isset($clickparams[1]) && is_numeric($clickparams[1])){
                        $visitorid = $clickparams[1];
                    }
                    if (isset($clickparams[2]) && is_numeric($clickparams[2])){
                        $clickid = $clickparams[2];
                    }
                    if (isset($clickparams[3]) && is_numeric($clickparams[3])){
                        $widgetid = $clickparams[3];
                    }
                    if (isset($clickparams[4]) && is_numeric($clickparams[4])){
                        $pageid = $clickparams[4];
                    }

                    if ($siteid == $affiliatesite || $siteid == false) {

                        $addconversion = false;

                        if (!$sale = Sale::get()->filter(array('TransactionId' => $node->ID, 'AffiliateNetwork' => "TradeTracker"))->first()) {

                            $sale = new Sale();

                            if ($visitorid && $visitordata = Visitor::get()->byID($visitorid)) {

                                $sale->VisitorID = $visitorid;
                                $sale->TrackType = 'Normal';

                                if (($visitordata->Source == "BingAds" || $visitordata->Source == "GoogleAds") && $visitordata->SourceClickId){

                                    //$addconversion = true;

                                }

                                if (!is_numeric($clickid) && $widgetid > 0 && $pageid > 0) {

                                    $widgettitle = "unknown widget";
                                    if ($widget = Widget::get()->byID($widgetid)){
                                        $widgettitle = $widget->Title;
                                    }

                                    $click = new Click();
                                    if ($page = SiteTree::get()->byID($pageid)) {
                                        $click->RefPage = $page->Link();
                                    }
                                    $click->ClickDate = $node->assessmentDate;
                                    $click->Action = "Click from widget: ".$widgettitle;
                                    $click->VisitorID = $visitorid;
                                    $click->write();

                                    $sale->ClickID = $click->ID;

                                }else{

                                    $sale->ClickID = $clickid;

                                }

                            } else {

                                $noVisitorId = true;
                                $sale->TrackType = 'UnknownVisitor';

                            }

                            $sale->TransactionId = $node->ID;
                            $sale->AffiliateNetwork = "TradeTracker";

                        }

                        $sale->SaleDate = $node->registrationDate;
                        //$sale->DeviceType = $node->device_type;
                        $sale->ProgramId = $node->campaign->ID;
                        $sale->ProgramName = $node->campaign->name;
                        $sale->Product = $node->campaignProduct->name;
                        $sale->Status = $status;
                        $sale->ApprovalDate = $node->assessmentDate;
                        $sale->DisapprovedReason = $node->rejectionReason;
                        $sale->Commission = $node->commission;

                        $sale->ClickDate = $node->originatingClickDate;
                        $sale->SubId1 = $node->reference;

                        $sale->write();

                        if ($addconversion == true){
                            $this->addConversion($visitordata->Source, $visitordata->SourceClickId, $node->registrationDate, $node->commission);
                        }

                        if ($noVisitorId == true) {

                            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Sale kon niet aan visitor worden gekoppeld", "SaleID: " . $sale->ID);
                            $email->sendPlain();

                        }

                    }

                }

            }

        }

    }

    public function AwinSales($datetype = 'transaction') {

        $affdetails = Config::inst()->get('AffiliateDetails');

        $aw_publisher_id = $affdetails['aw_publisher_id'];
        $aw_api_key = $affdetails['aw_api_key'];
        $affiliatesite = $affdetails['AffiliateSite'];

        $lastrun = CronLog::get()->first();
        $end = new \DateTime();
        $start = new \DateTime($lastrun->LastRun);
        $start->modify('-24 hours');
        //$start->modify('-10 days');

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'https://api.awin.com/publishers/'.$aw_publisher_id.'/transactions/?dateType='.$datetype.'&startDate='.$start->format('Y-m-d\TH%3\Ai%3\As').'&endDate='.$end->format('Y-m-d\TH%3\Ai%3\As').'&timezone=Europe/Paris&accessToken='.$aw_api_key, [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);

        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Awin Sales cron", $e->getMessage());
            $email->sendPlain();
            $error = true;
        }
        if (!isset($error)) {

            $msgs = json_decode($response->getBody());

            if (!empty($msgs)) {

                foreach ($msgs as $node) {

                    $noVisitorId = false;

                    $product = '';
                    foreach ($node->transactionParts as $part) {

                        $product .= $part->commissionGroupName." | ".$part->commissionAmount."\n";

                    }

                    $subid['clickRef'] = '';
                    $subid['clickRef2'] = '';
                    $subid['clickRef3'] = '';
                    foreach ($node->clickRefs as $key => $value) {
                        $subid[$key] = $value;
                    }

                    $SubId1 = '';
                    $SubId2 = '';
                    $SubId3 = '';
                    if (isset($subid['clickRef'])){
                        $SubId1 = $subid['clickRef'];
                    }
                    if (isset($subid['clickRef2'])){
                        $SubId2 = $subid['clickRef2'];
                    }
                    if (isset($subid['clickRef3'])){
                        $SubId3 = $subid['clickRef3'];
                    }

                    $siteid = false;
                    $visitorid = false;
                    $clickid = false;
                    $widgetid = false;
                    $pageid = false;

                    $clickparams = $this->getClickParams($SubId1.$SubId2.$SubId3);

                    if (isset($clickparams[0]) && is_numeric($clickparams[0])){
                        $siteid = $clickparams[0];
                    }
                    if (isset($clickparams[1]) && is_numeric($clickparams[1])){
                        $visitorid = $clickparams[1];
                    }
                    if (isset($clickparams[2]) && is_numeric($clickparams[2])){
                        $clickid = $clickparams[2];
                    }
                    if (isset($clickparams[3]) && is_numeric($clickparams[3])){
                        $widgetid = $clickparams[3];
                    }
                    if (isset($clickparams[4]) && is_numeric($clickparams[4])){
                        $pageid = $clickparams[4];
                    }

                    if (!$visitorid && !$clickid && strlen($SubId1.$SubId2.$SubId3) > 30 && isset($affdetails['UseMobile0318']) && $affdetails['UseMobile0318'] == true) {

                        if ($click = $this->getMobile0318Sale($SubId1.$SubId2.$SubId3)) {

                            $siteid = $affiliatesite;
                            $visitorid = $click->VisitorID;
                            $clickid = $click->ID;

                        }

                    }

                    if (!$visitorid && !$clickid && strpos($SubId1.$SubId2.$SubId3, '_cors') == true && isset($affdetails['UseOverstappen']) && $affdetails['UseOverstappen'] == true) {

                        if ($click = $this->getOverstappenClick($node->clickDate)) {

                            $siteid = $affiliatesite;
                            $visitorid = $click->VisitorID;
                            $widgetid = 5;
                            $pageid = 8; // autoverzekering pagina kz

                        }

                    }

                    // check if transaction is not deleted earlier (because was for another site for example)
                    $deleted = DeletedSale::get()->filter(array('TransactionId' => $node->id, 'AffiliateNetwork' => 'Awin'))->count();

                    if ($deleted === 0 && ($siteid == $affiliatesite || ($siteid == false  && strpos($node->publisherUrl, '0318') === false && strpos($SubId1.$SubId2.$SubId3, '_cors') === false) || ($siteid == false  && isset($affdetails['UseMobile0318']) && $affdetails['UseMobile0318'] == true) || ($siteid == false  && isset($affdetails['UseOverstappen']) && $affdetails['UseOverstappen'] == true))) {

                        $addconversion = false;

                        if (!$sale = Sale::get()->filter(array('TransactionId' => $node->id, 'AffiliateNetwork' => "Awin"))->first()) {

                            $sale = new Sale();

                            $countprogram = AwinProgram::get()->filter('ProgramId', $node->advertiserId)->count();

                            $programname = "unknown";
                            if ($countprogram > 0){
                                $program = AwinProgram::get()->filter('ProgramId', $node->advertiserId)->first();
                                $programname = $program->Name;
                            }

                            $sale->ProgramName = $programname;

                            if ($visitorid && $visitordata = Visitor::get()->byID($visitorid)) {

                                $sale->VisitorID = $visitorid;
                                $sale->TrackType = 'Normal';

                                if (($visitordata->Source == "BingAds" || $visitordata->Source == "GoogleAds") && $visitordata->SourceClickId){

                                    //$addconversion = true;

                                }

                                if (!is_numeric($clickid) && $widgetid > 0 && $pageid > 0) {

                                    $widgettitle = "unknown widget";
                                    if ($widget = Widget::get()->byID($widgetid)){
                                        $widgettitle = $widget->Title;
                                    }

                                    $click = new Click();
                                    if ($page = SiteTree::get()->byID($pageid)) {
                                        $click->RefPage = $page->Link();
                                    }
                                    $click->ClickDate = $node->clickDate;
                                    $click->Action = "Click from widget: ".$widgettitle;
                                    $click->VisitorID = $visitorid;
                                    $click->write();

                                    $sale->ClickID = $click->ID;

                                }else{

                                    $sale->ClickID = $clickid;

                                }

                            } else {

                                $noVisitorId = true;
                                $sale->TrackType = 'UnknownVisitor';

                            }

                            $sale->TransactionId = $node->id;
                            $sale->AffiliateNetwork = "Awin";

                        }

                        switch($node->clickDevice){
                            case "Android Mobile":
                            case "iPhone":
                                $devicetype = "phone";
                                break;
                            case "Apple Mac":
                            case "Windows":
                                $devicetype = "pc";
                                break;
                            case "iPad":
                                $devicetype = "tablet";
                                break;
                            default:
                                $devicetype = '';
                                break;
                        }

                        switch($node->commissionStatus){
                            case "pending":
                                $status = "open";
                                break;
                            case "approved":
                                $status = "approved";
                                break;
                            case "declined":
                                $status = "disapproved";
                                break;
                            default:
                                $status = '';
                                break;
                        }

                        $sale->SaleDate = $node->transactionDate;
                        $sale->DeviceType = $devicetype;
                        $sale->ProgramId = $node->advertiserId;

                        $sale->Product = $product;
                        $sale->Status = $status;
                        $sale->ApprovalDate = $node->validationDate;
                        $sale->DisapprovedReason = $node->declineReason;

                        $commission = 0;
                        foreach ($node->commissionAmount as $key => $value) {
                            $data[$key] = $value;
                        }
                        if (isset($data['amount'])){
                            $commission = $data['amount'];
                        }

                        $sale->Commission = $commission;

                        $sale->ClickDate = $node->clickDate;
                        $sale->SubId1 = $SubId1;
                        $sale->SubId2 = $SubId2;
                        $sale->SubId3 = $SubId3;

                        $sale->write();

                        if ($addconversion == true){
                            $this->addConversion($visitordata->Source, $visitordata->SourceClickId, $node->transactionDate, $commission);
                        }

                        if ($noVisitorId == true) {

                            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Sale kon niet aan visitor worden gekoppeld", "SaleID: " . $sale->ID);
                            $email->sendPlain();

                        }

                    }

                }

            }

        }

    }

    public function Mobile0318Clicks() {

        $affdetails = Config::inst()->get('AffiliateDetails');

        $mobile0318_apikey = $affdetails['mobile0318_apikey'];

        $lastrun = CronLog::get()->first();
        $start = new \DateTime($lastrun->LastRun);
        $start->modify('-24 hours');

        $data = array();
        $data['apikey'] = $mobile0318_apikey;
        $data['startdate'] = $start->format('d-m-Y');
        $data_string = json_encode($data);

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('POST', 'https://mobiel.0318media.nl/api/whitelabel/clicks', [
                'body' => $data_string,
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Mobile0318 Clicks cron", $e->getMessage());
            $email->sendPlain();
            $error = true;
        }
        if (!isset($error)) {

            $msgs = json_decode($response->getBody());

            if (!empty($msgs)) {

                foreach ($msgs as $node) {

                    if (($visitor = Visitor::get()->filter(array('IpMd5' => $node->ipaddress))->last()) && Click::get()->filter(array('ClickRef' => $node->clickref))->count() === 0){

                        $click = new Click();
                        $click->ClickDate = $node->clicktime;
                        if (strlen($node->referrer) > 0){
                            $click->RefPage = strtok($node->referrer, '?');
                        }
                        $click->Action = "Click via Mobiel0318 to ".$node->merchant.": ".$node->brand." ".$node->devicelink;
                        $click->ClickRef = $node->clickref;
                        $click->Extra = "0318_".$node->devicelink;
                        $click->VisitorID = $visitor->ID;
                        $click->write();

                    }

                }

            }

        }

    }

    public function AwinProgram() {

        $affdetails = Config::inst()->get('AffiliateDetails');

        $aw_publisher_id = $affdetails['aw_publisher_id'];
        $aw_api_key = $affdetails['aw_api_key'];

        $client = new \GuzzleHttp\Client();
        try {
            //$response = $client->request('GET', 'https://services.daisycon.com/publishers/'.$dc_publisher_id.'/transactions?media_id='.$dc_media_id.'&date_modified_start='.$start->format('Y-m-d H:i:s').'&date_modified_end='.$end->format('Y-m-d'), [
            $response = $client->request('GET', 'https://api.awin.com/publishers/' . $aw_publisher_id . '/programmes/?relationship=joined&accessToken=' . $aw_api_key, [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);

        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at Awin Programmes cron", $e->getMessage());
            $email->sendPlain();
            $error = true;
        }
        if (!isset($error)) {

            $msgs = json_decode($response->getBody());

            if (!empty($msgs)) {

                foreach ($msgs as $node) {

                    if (!$program = AwinProgram::get()->filter(array('ProgramId' => $node->id))->first()) {

                        $program = new AwinProgram();

                    }

                    $program->ProgramId = $node->id;
                    $program->Name = $node->name;
                    $program->write();

                }

            }

        }

    }

    public function getClickParams($subid = null){

        if ($subid){


            $output = strstr($subid, '_qca'); //gets all text from needle on
            $output = str_replace('_qca', '', $output);
            $output = strstr($output, 'h_', true); //gets all text before needle

            $output = explode('_', $output);

            //if (!empty($output) && is_numeric($output[0]) && is_numeric($output[1])){
            if (!empty($output) && is_numeric($output[0])){

                return $output;

            }
            return false;

        }
        return false;

    }

    public function getMobile0318Sale($clickparams){

        if ($click = Click::get()->filter(array('ClickRef:PartialMatch' => $clickparams))->last()){

            return $click;

        }

        return false;

    }

    public function getOverstappenClick($clickdate){

        $start = new \DateTime($clickdate);
        $start->modify('-1 hour');
        $end = new \DateTime($clickdate);
        $end->modify('+1 minute');

        if ($click = Click::get()->filter(array('ClickDate:GreaterThan' => $start->format('Y-m-d H:i:s'), 'ClickDate:LessThan' => $end->format('Y-m-d H:i:s'), 'Action:StartsWith' => 'Overstappen.nl'))->sort('ClickDate')->last()){
            //if ($click = Click::get()->filter(array('Action:StartsWith' => 'Overstappen.nl'))->last()){

            return $click;

        }

        return false;

    }

    public function addConversion($source, $clickid, $saledate, $value){

        $time = new \DateTime($saledate);

        switch($source){
            case "BingAds":
                $conversion = new BingConversion();
                break;
            case "GoogleAds":
                $conversion = new GoogleConversion();
                break;
            default:
                return false;
        }

        $conversion->setConversionTime($time->format('Y-m-d H:i:s'));
        $conversion->setConversionValue($value);
        $conversion->setConversionClickId($clickid);

        try{
            $output = $conversion->add();
        } catch (\Exception $e){

            $email_subject = "addConversion error in: AffiliateTransactionCron->addConversion";
            $email_body = $e->getMessage()." | ConversionClickId: ".$clickid." | source: ".$source;

            if (Director::isDev()) {
                Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
            } else {
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                $email->sendPlain();
            }
            return false;

        }
        return $output;
    }

    public function DbCleanup(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips')) || Director::isDev()) {

            $recentdate = new \DateTime();
            $recentdate->modify('-2 days');

            $visitorips = DB::query("SELECT Created, Ip, COUNT(*) as count FROM HestecLinkManagerVisitor WHERE Created > '".$recentdate->format('Y-m-d H:i:s')."' GROUP BY Ip");

            foreach ($visitorips as $node){

                if ($node['count'] > 10) {
                    $ipcheckclicks = Visitor::get()->filter(array('Created:GreaterThan' => $recentdate->format('Y-m-d H:i:s'), 'Sales.Count():not' => 0, 'Clicks.Count():not' => 0, 'Ip' => $node['Ip']))->count();
                    if ($ipcheckclicks == 0) {
                        if ($ipchecknoclicks = Visitor::get()->filter(array('Created:GreaterThan' => $recentdate->format('Y-m-d H:i:s'), 'Sales.Count()' => 0, 'Clicks.Count()' => 0, 'Ip' => $node['Ip']))){
                            if ($ipchecknoclicks->count() > 10){
                                foreach ($ipchecknoclicks as $rm){
                                    $rm->delete();
                                }
                            }
                        }
                    }
                }

            }

            /*$boturls = array('http://ib.adnxs.com/cr?test=1');
            $botqueries = array('adroll_random=');

            $visitorsrecent = Visitor::get()->filter(array('Created:GreaterThan' => $recentdate->format('Y-m-d H:i:s'), 'Sales.Count()' => 0, 'Clicks.Count()' => 0))->filterAny(array('Referer:PartialMatch' => $boturls, 'QueryString:PartialMatch' => $botqueries));

            foreach ($visitorsrecent as $item) {

                $item->delete();

            }*/

            $cleandate = new \DateTime();
            $cleandate->modify('-3 months');

            $visitors = Visitor::get()->filter(array('Created:LessThan' => $cleandate->format('Y-m-d H:i:s'), 'Sales.Count()' => 0))->limit(5000);

            foreach ($visitors as $item) {

                $clicks = Click::get()->filter('VisitorID', $item->ID);

                foreach ($clicks as $clickitem) {

                    $clickitem->delete();

                }

                $item->delete();

            }

            return "success";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    // function for strpos array from: https://stackoverflow.com/questions/6284553/using-an-array-as-needles-in-strpos
    public function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

}
