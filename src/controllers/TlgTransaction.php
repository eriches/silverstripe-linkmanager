<?php

use SilverStripe\Control\Email\Email;
use Hestec\LinkManager\Sale;
use SilverStripe\Core\Config\Config;
use Hestec\LinkManager\Click;
use SilverStripe\ORM\DB;
use Hestec\LinkManager\TlgSale;
use Hestec\LinkManager\TlgType;
use SilverStripe\Control\Director;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Security\Security;

class TlgTransaction extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'Import',
        'test'
    );

    public function init() {
        parent::init();

        if (!Security::getCurrentUser()){
            return $this->httpError(404);
        }

    }

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            if ($click = Click::get()->filter(array('ClickDate:GreaterThan' => '2022-03-21 18:17:45', 'ClickDate:LessThan' => '2022-03-21 19:19:45', 'Action' => 'TLG Brommerverzekering vergelijker'))->sort('ClickDate')->last()) {

                return $click->ID;

            }



        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function Import(){

        $from = new \DateTime();
        if (isset($_GET['week'])){
            $from->modify('-7 days');
        }else{
            $from->modify('-1 year');
        }

        DB::query("DELETE FROM HestecLinkManagerTlgSale WHERE TransactionId IS NULL OR FirstClickTime IS NULL OR LastClickTime IS NULL OR SaleDate < '".$from->format('Y-m-d 00:00:00')."'");

        $tlgsales = TlgSale::get()->sort('ID')->limit(50);

        foreach ($tlgsales as $tlg){

            $click_error = false;
            $noVisitorId = false;
            $new_disapproved = false;
            if (!$sale = Sale::get()->filter(array('TransactionId' => $tlg->TransactionId, 'AffiliateNetwork' => "TLG"))->first()) {

                $sale = new Sale();
                $sale->TransactionId = $tlg->TransactionId;
                $sale->AffiliateNetwork = "TLG";
                $sale->SaleDate = $tlg->SaleDate;

                $commissiontype = strtolower($tlg->CommissionTypeName);

                if ($click_data = $this->getClick($tlg->FirstClickTime, $tlg->LastClickTime, $commissiontype)) {

                    if (isset($click_data['Error']) && $click_data['Error'] == "noSearchAction"){

                        $email_subject = "TLG Sale: geen SearchType gevonden";
                        $email_body = "Er is geen searchtype gevonden voor TLG sale #" . $tlg->TransactionId ." met CommissionTypeName: ".$commissiontype.". Deze sale is daarom nog niet weggeschreven, voeg eerst een searchtype toe in TlgType.";

                        if (Director::isDev()) {
                            Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
                        } else {
                            $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                            $email->sendPlain();
                        }
                        $click_error = true;

                    }else {

                        $click = Click::get()->byID($click_data['ClickID']);

                        $sale->VisitorID = $click->VisitorID;
                        $sale->TrackType = 'Normal';

                        $addclick = new Click();
                        $addclick->ClickDate = $click_data['ClickDate'];
                        $addclick->Action = "Click from TLG vergelijker";
                        $addclick->VisitorID = $click->VisitorID;
                        $addclick->write();

                        $sale->ClickID = $addclick->ID;
                        $sale->ClickDate = $click_data['ClickDate'];

                    }

                }else{

                    $noVisitorId = true;
                    $sale->ClickDate = $tlg->FirstClickTime;
                    $sale->TrackType = 'UnknownVisitor';

                }

                // if a new sale is already disapproved, no need to add
                if ($tlg->Status == "D"){
                    $new_disapproved = true;
                }

            }

            if (($sale->Status == "open" || empty($sale->Status)) && $click_error == false && $new_disapproved == false){

                $sale->Commission = $tlg->Commission;
                $sale->ProgramName = $tlg->CampaignName;
                $sale->Product = $tlg->CampaignName." ".$tlg->CommissionTypeName." ".$tlg->ProductId."\nOrderID: ".$tlg->OrderId;

                switch($tlg->Status){
                    case "P":
                        $status = "open";
                        break;
                    case "A":
                        $status = "approved";
                        break;
                    case "D":
                        $status = "disapproved";
                        break;
                    default:
                        $status = '';
                        break;
                }

                $sale->Status = $status;

                $sale->write();

                if ($noVisitorId == true) {
                    $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "TLG Sale kon niet aan visitor worden gekoppeld", "SaleID: " . $sale->ID);
                    $email->sendPlain();
                }

            }

            if ($click_error == false){
                $tlg->delete();
            }

        }

        $db_count = TlgSale::get()->count();

        return "Nog ".$db_count." sales in de importdatabase.";

    }

    public function getClick($firstclickdate, $lastclickdate, $commissiontype){

        $tlgtypes = TlgType::get()->sort('Sort');

        foreach ($tlgtypes as $type){

            if ($this->strposa($commissiontype, $type->SearchTypes->Value) !== false){

                $search_action = $type->ClickAction;
                break;

            }

        }

        if (isset($search_action)) {
            $start = new \DateTime($firstclickdate);
            $start->modify('-1 hour');
            $end = new \DateTime($firstclickdate);
            $end->modify('+2 minutes');

            if ($click = Click::get()->filter(array('ClickDate:GreaterThan' => $start->format('Y-m-d H:i:s'), 'ClickDate:LessThan' => $end->format('Y-m-d H:i:s'), 'Action' => $search_action))->sort('ClickDate')->last()) {

                return array(
                    'ClickID' => $click->ID,
                    'ClickDate' => $firstclickdate
                );

            }

            $start2 = new \DateTime($lastclickdate);
            $start2->modify('-1 hour');
            $end2 = new \DateTime($lastclickdate);
            $end2->modify('+2 minutes');

            if ($click = Click::get()->filter(array('ClickDate:GreaterThan' => $start2->format('Y-m-d H:i:s'), 'ClickDate:LessThan' => $end2->format('Y-m-d H:i:s'), 'Action' => $search_action))->sort('ClickDate')->last()) {

                return array(
                    'ClickID' => $click->ID,
                    'ClickDate' => $lastclickdate
                );

            }
        }else{

            return array(
                'Error' => "noSearchAction"
            );

        }

        return false;

    }

    // function for strpos array from: https://stackoverflow.com/questions/6284553/using-an-array-as-needles-in-strpos
    public function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

}
