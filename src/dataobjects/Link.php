<?php

namespace Hestec\LinkManager;

use SilverStripe\Forms\TextareaField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Control\Director;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\CheckboxField;

class Link extends DataObject {

    private static $singular_name = 'Link';
    private static $plural_name = 'Links';

    private static $table_name = 'HestecLinkManagerLink';

    private static $db = array(
        'AffiliateNetwork' => "Enum('Daisycon,Awin,TradeTracker,Tradedoubler,Adtraction,CJ,Partnerize,Bol,Coolblue,Amazon,GetYourGuide,Tiqets,Whitelabeled,Booking,Skyscanner,other','')",
        'Url' => 'Text',
        'SeoUrl' => 'Varchar(255)',
        'Title' => 'Varchar(255)',
        'InternTitle' => 'Varchar(255)',
        'Description' => 'Text',
        'Enabled' => 'Boolean'
    );

    private static $defaults = array(
        'Enabled' => true
    );

    private static $has_many = [
        'Clicks' => Click::class
    ];

    private static $summary_fields = array(
        'ID',
        'Enabled.Nice',
        'AffiliateNetwork',
        'Url',
        'Title',
        'InternTitle'
    );

    public function getCMSFields() {

        $EnabledField = CheckboxField::create('Enabled', "Enabled");

        $TradeTrackerInfoText = "<p>Als de url voor TradeTracker het domein tradetracker.net is, dan de ref toevoegen als: 'r=TTSUBIDHESTEC'.<br>";
        $TradeTrackerInfoText .= "Als de url voor TradeTracker het domein van de adverteeerder is, dan de ref toevoegen als: '?tt=4113_144851_318703_TTSUBIDHESTEC', ";
        $TradeTrackerInfoText .= "d.w.z.: aan de tt paramater '_TTSUBIDHESTEC' toevoegen.<br>";
        $TradeTrackerInfoText .= "Deze ref wordt in de linkcontroller met str_replace vervangen door de juiste visitor-ref.</p>";

        $AmazonInfoText = "<p>Als er spaties staan in de Amazon link (bijvoorbeeld bij keywords), vervang deze dan door een '+'.</p>";

        $TradeTrackerHeaderField = HeaderField::create('TradeTrackerHeader', "TradeTracker toelichting");
        $TradeTrackerInfoField = LiteralField::create('TradeTrackerInfo', $TradeTrackerInfoText);
        $AmazonHeaderField = HeaderField::create('AmazonHeader', "Amazon toelichting");
        $AmazonInfoField = LiteralField::create('AmazonInfo', $AmazonInfoText);

        $UrlField = TextareaField::create('Url', 'Url');
        $UrlField->setRows(2);
        $UrlField->setDescription("For Daisycon without the <i>ws</i> parameter. For Awin without the <i>clickref</i> parameter. For TradeTracker add TTSUBIDHESTEC as ref. For Bol.com without the <i>subid</i> parameter. ");
        $SeoUrlField = TextField::create('SeoUrl', 'SeoUrl');
        $TitleField = TextField::create('Title', 'Title');
        $InternTitleField = TextField::create('InternTitle', 'InternTitle');
        $DescriptionField = TextareaField::create('Description', 'Description');
        $AffiliateNetworkField = DropdownField::create('AffiliateNetwork', 'AffiliateNetwork', $this->dbObject('AffiliateNetwork')->enumValues());
        $AffiliateNetworkField->setEmptyString("(select)");

        return new FieldList(
            $EnabledField,
            $AffiliateNetworkField,
            $UrlField,
            $SeoUrlField,
            $TitleField,
            $InternTitleField,
            $DescriptionField,
            $TradeTrackerHeaderField,
            $TradeTrackerInfoField,
            $AmazonHeaderField,
            $AmazonInfoField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'AffiliateNetwork',
            'Url',
            'SeoUrl',
            'Title',
            'InternTitle'
        ));
    }

    public function validate()
    {
        $result = parent::validate();

        $dcurls = DaisyconUrl::get();

        $daisyconurl = array();
        foreach ($dcurls as $url){

            array_push($daisyconurl, $url->Domain);

        }

        $awinurl = array('awin1.com');
        $bookingurl = array('booking.com');
        $bolurl = array('partner.bol.com');
        $tradedoublerurl = array('clk.tradedoubler.com');
        $skyscannerurl = array('skyscanner.net', 'skyscanner.pxf.io');
        $whitelabeledurl = array('api-energie.whitelabeled.nl', 'api-internet.whitelabeled.nl', 'api-mobile.whitelabeled.nl');
        $tradetrackerurl = array('tradetracker.net');
        $tradetrackerquery = array('tt=', 'r=');

        if (!filter_var($this->Url, FILTER_VALIDATE_URL)){
            $result->addError('This is not a valid url.');
        }
        if ($this->AffiliateNetwork === 'Daisycon' && strpos($this->Url, '&ws=') !== false){
            $result->addError('Remove the ws parameter from the URL.');
        }
        if ($this->AffiliateNetwork === 'Awin' && strpos($this->Url, '&clickref=') !== false){
            $result->addError('Remove the clickref parameter from the URL.');
        }
        if ($this->AffiliateNetwork === 'Bol' && strpos($this->Url, '&subid=') !== false){
            $result->addError('Remove the subid parameter from the URL.');
        }
        if ($this->AffiliateNetwork === 'Whitelabeled' && strpos($this->Url, '&t=') !== false){
            $result->addError('Remove the t parameter from the URL.');
        }
        if ($this->AffiliateNetwork === 'Daisycon' && $this->strposa($this->Url, $daisyconurl) === false){
            $result->addError("This doesn't seem like a Daisycon URL.");
        }
        if ($this->AffiliateNetwork === 'Awin' && $this->strposa($this->Url, $awinurl) === false){
            $result->addError("This doesn't seem like an Awin URL.");
        }
        if ($this->AffiliateNetwork === 'Bol' && $this->strposa($this->Url, $bolurl) === false){
            $result->addError("This doesn't seem like an Bol.com URL.");
        }
        if ($this->AffiliateNetwork === 'Tradedoubler' && $this->strposa($this->Url, $tradedoublerurl) === false){
            $result->addError("This doesn't seem like an Tradedoubler URL.");
        }
        if ($this->AffiliateNetwork === 'Skyscanner' && $this->strposa($this->Url, $skyscannerurl) === false){
            $result->addError("This doesn't seem like an Skyscanner URL.");
        }
        if ($this->AffiliateNetwork === 'Booking' && $this->strposa($this->Url, $bookingurl) === false){
            $result->addError("This doesn't seem like an Booking.com URL.");
        }
        if ($this->AffiliateNetwork === 'Whitelabeled' && $this->strposa($this->Url, $whitelabeledurl) === false){
            $result->addError("This doesn't seem like an Whitelabeled URL.");
        }
        if ($this->AffiliateNetwork === 'TradeTracker' && $this->strposa($this->Url, $tradetrackerurl) === false && $this->strposa($this->Url, $tradetrackerquery) === false ){
            $result->addError("This doesn't seem like a TradeTracker URL.");
        }
        if ($this->AffiliateNetwork === 'TradeTracker' && str_contains($this->Url, "TTSUBIDHESTEC") === false){
            $result->addError("The ref TTSUBIDHESTEC must be added.");
        }

        return $result;
    }

    // function for strpos array from: https://stackoverflow.com/questions/6284553/using-an-array-as-needles-in-strpos
    public function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

    public function onBeforeWrite()
    {

        $this->SeoUrl = SiteTree::create()->generateURLSegment($this->SeoUrl);

        parent::onBeforeWrite();

    }

    public function RedirectLink(){

        return Director::absoluteBaseUrl()."link/id/".$this->ID."/".$this->SeoUrl;

    }

    public function TitleInternTitle(){

        if (isset($this->InternTitle)){

            return $this->Title." (".$this->InternTitle.")";

        }

        return $this->Title;

    }

}