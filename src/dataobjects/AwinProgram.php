<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;

class AwinProgram extends DataObject {

    private static $table_name = 'HestecLinkManagerAwinProgram';

    private static $db = [
        'ProgramId' => 'Int',
        'Name' => 'Varchar(255)'
    ];

}