<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;

class ApiToken extends DataObject {

    private static $table_name = 'HestecLinkManagerApiToken';

    private static $db = [
        'DcAccessToken' => 'Text',
        'DcAccessTokenExpire' => 'Datetime',
        'DcRefreshToken' => 'Text',
    ];

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (ApiToken::get()->count() == 0){

            $record = new ApiToken();
            $record->write();

        }

    }

}