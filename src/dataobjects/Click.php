<?php

namespace Hestec\LinkManager;

use SilverStripe\Forms\DatetimeField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Permission;
use SilverStripe\ORM\FieldType\DBField;

class Click extends DataObject {

    private static $table_name = 'HestecLinkManagerClick';

    private static $db = [
        'ClickDate' => 'Datetime',
        'RefPage' => 'Varchar(255)',
        'Action' => 'Varchar(255)',
        'Vars' => 'Text',
        'ClickRef' => 'Varchar(255)',
        'Extra' => 'Varchar(255)',
        'Ip' => 'Varchar(50)',
        'HttpUserAgent' => 'Varchar(255)',
        'Referer' => 'Varchar(255)'
    ];

    private static $default_sort='ClickDate ASC';

    private static $has_one = [
        'Visitor' => Visitor::class,
        'Link' => Link::class
    ];

    private static $has_many = [
        'Sales' => Sale::class
    ];

    private static $summary_fields = array(
        'Visitor.ID' => 'Visitor',
        'ClickDate' => 'Time',
        'Action' => 'Action',
        'RefPage' => 'RefPage',
        'isSale' => 'Sale'
    );

    public function getCMSFields() {

        $VisitorIDField = NumericField::create('VisitorID', "VisitorID");
        $VisitorIDField->setReadonly(true);
        $ClickDateField = DatetimeField::create('ClickDate', 'ClickDate');
        $ClickDateField->setReadonly(true);
        $ActionField = TextField::create('Action', "Action");
        $ActionField->setReadonly(true);
        $RefPageField = TextField::create('RefPage', "RefPage");
        $RefPageField->setReadonly(true);
        $VarsField = TextareaField::create('Vars', "Vars");
        $VarsField->setReadonly(true);

        if ($this->VisitorID > 0){

            $VisitorDetailsHeaderField = HeaderField::create('VisitorDetailsHeader', "Visitor details (afkomstig vanuit de Visitor tabel)");
            $VisitorRefererField = TextField::create('VisitorReferer', "Referer");
            $VisitorRefererField->setValue($this->Visitor()->Referer);
            $VisitorRefererField->setReadonly(true);
            $VisitorIpField = TextField::create('VisitorIp', "Ip");
            $VisitorIpField->setValue($this->Visitor()->Ip);
            $VisitorIpField->setReadonly(true);
            $HttpUserAgentField = TextField::create('VisitorHttpUserAgent', "HttpUserAgent");
            $HttpUserAgentField->setValue($this->Visitor()->HttpUserAgent);
            $HttpUserAgentField->setReadonly(true);

        }else{

            $VisitorDetailsHeaderField = HeaderField::create('VisitorDetailsHeader', "Visitor details (afkomstig vanuit de Click tabel)");
            $VisitorRefererField = TextField::create('Referer', "Referer");
            $VisitorRefererField->setReadonly(true);
            $VisitorIpField = TextField::create('Ip', "Ip");
            $VisitorIpField->setReadonly(true);
            $HttpUserAgentField = TextField::create('HttpUserAgent', "HttpUserAgent");
            $HttpUserAgentField->setReadonly(true);

        }

        return new FieldList(
            $VisitorIDField,
            $ClickDateField,
            $ActionField,
            $RefPageField,
            $VarsField,
            $VisitorDetailsHeaderField,
            $VisitorRefererField,
            $VisitorIpField,
            $HttpUserAgentField
        );

    }

    public function onBeforeWrite() {

        if (!$this->ClickDate){

            $click = new \DateTime();

            $this->ClickDate = $click->format('Y-m-d H:i:s');

        }

        parent::onBeforeWrite();

    }

    public function isSale(){

        if ($this->Sales()->Count() > 0){

            return DBField::create_field('HTMLText', "SALE");

        }

        return false;

    }

    public function ClickDateAction(){

        /*if ($this->Sales()->Count() > 0){

            return DBField::create_field('HTMLText', "SALE");

        }*/

        return $this->ClickDate." - ".$this->VisitorID." - ".$this->Action." - ".$this->RefPage;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return false;
    }

    public function canDelete($member = null)
    {
        return false;
    }

    public function canCreate($member = null, $context = [])
    {
        return false;
    }

}