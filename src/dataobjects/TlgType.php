<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;
use Symbiote\MultiValueField\Fields\MultiValueTextField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;

class TlgType extends DataObject {

    private static $table_name = 'HestecLinkManagerTlgType';

    private static $db = [
        'Title' => 'Varchar(255)',
        'ClickAction' => 'Varchar(255)',
        'SearchTypes' => 'MultiValueField',
        'Sort' => 'Int'
    ];

    private static $default_sort='Sort';

    private static $summary_fields = array(
        'Title' => 'Title',
        'ClickAction' => 'ClickAction'
    );

    public function getCMSFields() {

        $TitleField = TextField::create('Title', 'Title');
        $TitleField->setDescription("Alleen voor een titel hier, wordt niet gebruikt in het systeem.");
        $ClickActionField = TextField::create('ClickAction', 'ClickAction');
        $ClickActionField->setDescription("De action waar op gezocht moet worden in de clicks.");
        $SearchTypesField = MultiValueTextField::create('SearchTypes', "SearchTypes");
        $SearchTypesField->setDescription("De commissionTypeName (Type in de webomgeving) van TLG.");

        return new FieldList(
            $TitleField,
            $ClickActionField,
            $SearchTypesField
        );

    }

}