<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;

class TlgSale extends DataObject {

    private static $table_name = 'HestecLinkManagerTlgSale';

    private static $db = [
        'TransactionId' => 'Varchar(255)',
        'Commission' => 'Currency',
        'OrderId' => 'Varchar(255)',
        'ProductId' => 'Varchar(255)',
        'SaleDate' => 'Datetime',
        'CampaignName' => 'Varchar(255)',
        'CommissionTypeName' => 'Varchar(255)',
        'Status' => 'Varchar(1)',
        'FirstClickTime' => 'Datetime',
        'LastClickTime' => 'Datetime'
    ];

    private static $summary_fields = array(
        'TransactionId' => 'TransactionId',
        'Commission.Nice' => 'Commission',
        'SaleDate' => 'SaleDate',
        'CampaignName' => 'CampaignName',
        'CommissionTypeName' => 'CommissionTypeName',
        'Status' => 'Status',
        'FirstClickTime' => 'FirstClickTime',
        'LastClickTime' => 'LastClickTime'
    );

}