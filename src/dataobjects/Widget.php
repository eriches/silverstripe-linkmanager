<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;

class Widget extends DataObject {

    private static $table_name = 'HestecLinkManagerWidget';

    private static $db = [
        'Title' => 'Varchar(255)'
    ];

}