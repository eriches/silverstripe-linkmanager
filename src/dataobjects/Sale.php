<?php

namespace Hestec\LinkManager;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DatetimeField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CurrencyField;
use Hestec\GoogleBingAdsApi\BingConversion;
use Hestec\GoogleBingAdsApi\GoogleConversion;
use Sheadawson\DependentDropdown\Forms\DependentDropdownField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Control\Director;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Email\Email;
use SilverStripe\Core\Config\Config;

class Sale extends DataObject {

    private static $table_name = 'HestecLinkManagerSale';

    private static $db = [
        'TransactionId' => 'Varchar(255)',
        'AffiliateNetwork' => "Enum('Daisycon,Awin,TradeTracker,Partnerize,CJ,TLG,Bol,Tradedoubler,Adtraction,other','')",
        'SaleDate' => 'Datetime',
        'ClickDate' => 'Datetime',
        'DeviceType' => "Enum('phone,pc,tablet','')",
        'ProgramId' => 'Int',
        'ProgramName' => 'Varchar(255)',
        'Product' => 'Text',
        'Status' => "Enum('open,approved,disapproved','')",
        'ApprovalDate' => 'Datetime',
        'DisapprovedReason' => 'Varchar(255)',
        'Commission' => 'Currency',
        'SubId1' => 'Varchar(255)',
        'SubId2' => 'Varchar(255)',
        'SubId3' => 'Varchar(255)',
        'TrackType' => "Enum('Normal,MarkedAsBot,UnknownVisitor,Bonus,Unknown,other','')",
        'ConversionAdded' => 'Boolean',
        'Manual' => 'Boolean',
        'AddNewClick' => 'Boolean'
    ];

    private static $default_sort='SaleDate DESC';

    private static $defaults = array(
        'Status' => "open"
    );

    private static $has_one = [
        'Visitor' => Visitor::class,
        'Click' => Click::class
    ];

    private static $searchable_fields = [
        'SaleDate',
        'Status',
        'Visitor.Source',
        'Manual'
    ];

    private static $summary_fields = array(
        'SaleDate' => 'SaleDate',
        'Commission.Nice' => 'Commission',
        'Status' => 'Status',
        'ProgramName' => 'ProgramName',
        'Visitor.Source' => 'Source',
        'getReferer' => 'Referer',
        'Visitor.Device' => 'Device',
        'Visitor.Browser' => 'Browser'
    );

    public function getCMSFields() {

        if ($this->ID) {

            $ClicksGridField = GridField::create(
                'Clicks',
                'Clicks',
                $this->Visitor()->Clicks(),
                GridFieldConfig_RecordEditor::create()
            //->addComponent(new GridFieldOrderableRows())
            );

            $VisitorIDField = NumericField::create('VisitorID', "VisitorID");
            $AllSubIdsField = TextField::create('AllSubIds', "AllSubIds");
            $AllSubIdsField->setValue($this->SubId1 . $this->SubId2 . $this->SubId3);
            $AllSubIdsField->setReadonly(true);
            if ($this->VisitorID > 0 && $this->TrackType == "Normal") {
                $TrackTypeField = TextField::create('TrackType', "TrackType");
                $TrackTypeField->setReadonly(true);
                $VisitorIDField->setReadonly(true);
            } else {
                $TrackTypeField = DropdownField::create('TrackType', "TrackType", $this->dbObject('TrackType')->enumValues());
            }
            $CommissionField = CurrencyField::create('Commission', 'Commission');
            $SaleDateField = DatetimeField::create('SaleDate', 'SaleDate');
            $ClickDateField = DatetimeField::create('ClickDate', 'ClickDate');
            if ($this->Manual == true) {
                $StatusField = DropdownField::create('Status', "Status", $this->dbObject('Status')->enumValues());
            }else{
                $StatusField = TextField::create('Status', "Status");
                $StatusField->setReadonly(true);
            }
            $ProgramNameField = TextField::create('ProgramName', 'ProgramName');
            $AffiliateNetworkField = TextField::create('AffiliateNetwork', 'AffiliateNetwork');
            $ProductField = TextareaField::create('Product', 'Product');
            $VisitorRefererField = TextField::create('VisitorReferer', "Referer");
            $VisitorRefererField->setValue($this->Visitor()->Referer);
            $VisitorRefererField->setReadonly(true);
            $VisitorEntryField = TextField::create('VisitorEntry', "Entry");
            $VisitorEntryField->setValue($this->Visitor()->Entry);
            $VisitorEntryField->setReadonly(true);

            if ($this->ID) {
                $SaleDateField->setReadonly(true);
                $CommissionField->setReadonly(true);
                $ClickDateField->setReadonly(true);
                $ProgramNameField->setReadonly(true);
                $AffiliateNetworkField->setReadonly(true);
                $ProductField->setReadonly(true);
            }

            return new FieldList(
                $VisitorIDField,
                $AllSubIdsField,
                $TrackTypeField,
                $SaleDateField,
                $ClickDateField,
                $CommissionField,
                $StatusField,
                $AffiliateNetworkField,
                $ProgramNameField,
                $ProductField,
                $VisitorRefererField,
                $VisitorEntryField,
                $ClicksGridField
            );

        }else{

            //$created = new \DateTime();
            //$created->modify('-10 weeks');

            //$VisitorSource = Visitor::get()->filter(array('Created:GreaterThanOrEqual' => $created->format('Y-m-d 00:00:00')))->map('ID', 'ID');

            /*$VisitorSource = function($val) {
                $createdup = new \DateTime($val);
                $createdup->modify('-1 hour');
                $createddown = new \DateTime($val);
                $createddown->modify('+5 minutes');

                return Visitor::get()->filter(array('Created:GreaterThanOrEqual' => $createdup->format('Y-m-d H:i:s'), 'Created:LessThanOrEqual' => $val, 'Clicks.Count():GreaterThan' => 0))->map('ID', 'ID');
            };*/

            $ClickSource = function($val) {
                $createdup = new \DateTime($val);
                $createdup->modify('-1 hour');
                $createddown = new \DateTime($val);
                $createddown->modify('+5 minutes');

                return Click::get()->filter(array('ClickDate:GreaterThanOrEqual' => $createdup->format('Y-m-d H:i:s'), 'ClickDate:LessThanOrEqual' => $createddown->format('Y-m-d H:i:s'), 'VisitorID:not' => 0))->sort('ClickDate DESC')->map('ID', 'ClickDateAction');
            };

            /*$ClickSource = function($val) {
                return Click::get()->filter(array('VisitorID' => $val))->map('ID', 'ClickDateAction');
            };*/

            $ClickDateField = DatetimeField::create('ClickDate', "ClickDate");
            $SaleDateField = DatetimeField::create('SaleDate', "SaleDate");
            //$VisitorField = DependentDropdownField::create('VisitorID', "Visitor", $VisitorSource);
            //$VisitorField->setDepends($ClickDateField);
            //$VisitorField->setEmptyString("(select)");
            $AffiliateNetworkField = DropdownField::create('AffiliateNetwork', "AffiliateNetwork", $this->dbObject('AffiliateNetwork')->enumValues());
            $AffiliateNetworkField->setEmptyString("(select)");
            $TransactionIdField = TextField::create('TransactionId', "TransactionId");
            $ClickField = DependentDropdownField::create('ClickID', "Click", $ClickSource);
            $ClickField->setDepends($ClickDateField);
            $ClickField->setEmptyString("(select)");
            $AddNewClickField = CheckboxField::create('AddNewClick', "AddNewClick");
            $AddNewClickField->setDescription("Als je een nieuwe click wilt toevoegen met de Clickdate. Anders wordt de sale gekoppeld aan de geselecteerde Click.");
            $StatusField = DropdownField::create('Status', "Status", $this->dbObject('Status')->enumValues());
            $CommissionField = CurrencyField::create('Commission', "Commission");
            $ProductField = TextareaField::create('Product', "Product");
            $ManualField = HiddenField::create('Manual', "Manual", true);

            return new FieldList(
                $ManualField,
                $ClickDateField,
                $ClickField,
                $AddNewClickField,
                $SaleDateField,
                $AffiliateNetworkField,
                $TransactionIdField,
                $StatusField,
                $CommissionField,
                $ProductField
            );


        }

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'ClickDate',
            'ClickID',
            'SaleDate',
            'AffiliateNetwork',
            'Status'
        ));
    }

    public function onBeforeDelete(){

        $deleted = new DeletedSale();
        $deleted->TransactionId = $this->TransactionId;
        $deleted->AffiliateNetwork = $this->AffiliateNetwork;
        $deleted->write();

        parent::onBeforeDelete();

    }

    protected function onBeforeWrite()
    {

        if (!$this->ID && $this->Manual == true){

            $this->VisitorID = $this->Click()->VisitorID;
            $this->ProgramName = $this->AffiliateNetwork;

            if ($this->AddNewClick == true){

                $click = new Click();
                $click->ClickDate = $this->ClickDate;
                $click->Action = "unknown (manual added)";
                $click->VisitorID = $this->Click()->VisitorID;
                $click->write();

                $this->ClickID = $click->ID;

            }

        }

        parent::onBeforeWrite(); // TODO: Change the autogenerated stub
    }

    public function onAfterWrite()
    {
        // this is not running in DEV mode, see the if statement: Director::isLive()
        if (($this->Visitor()->Source == "BingAds" || $this->Visitor()->Source == "GoogleAds") && strlen($this->Visitor()->SourceClickId) > 10 && $this->ConversionAdded == false && Director::isLive()) {

            $output = $this->addConversion($this->Visitor()->Source, $this->Visitor()->SourceClickId, $this->SaleDate, $this->Commission);

            if ($output == true) {
                $this->ConversionAdded = true;
                $this->write();
            }

        }

        parent::onAfterWrite(); // TODO: Change the autogenerated stub
    }

    public function getReferer(){

        $visitor = $this->Visitor();

        if (filter_var($visitor->Referer, FILTER_VALIDATE_URL)) {

            $output = parse_url($visitor->Referer, PHP_URL_HOST);
            return DBField::create_field('HTMLText', $output);

        }
        return false;

    }

    public function addConversion($source, $clickid, $saledate, $value){

        $time = new \DateTime($saledate);

        switch($source){
            case "BingAds":
                $conversion = new BingConversion();
                break;
            case "GoogleAds":
                $conversion = new GoogleConversion();
                break;
            default:
                return false;
        }

        // if there are multiple sales from 1 visitor at the same time prevent timestamp conflict
        if ($modify_seconds = Sale::get()->filter(array('VisitorID' => $this->VisitorID, 'SaleDate' => $time->format('Y-m-d H:i:s')))->count()) {
            if ($modify_seconds > 0) {
                $time->modify('+'.$modify_seconds.' seconds');
            }
        }

        $conversion->setConversionTime($time->format('Y-m-d H:i:s'));
        $conversion->setConversionValue($value);
        $conversion->setConversionClickId($clickid);

        try{
            $output = $conversion->add();
        } catch (\Exception $e){

            $email_subject = "addConversion error in: AffiliateTransactionCron->addConversion";
            $email_body = $e->getMessage()." | ConversionClickId: ".$clickid." | source: ".$source;

            if (Director::isDev()) {
                Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
            } else {
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                $email->sendPlain();
            }
            return false;

        }
        return $output;
    }

}
