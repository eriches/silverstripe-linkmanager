<?php

namespace Hestec\LinkManager;

use SilverStripe\Forms\ReadonlyField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\ORM\FieldType\DBField;

class AdsCost extends DataObject {

    private static $singular_name = 'AdsCost';
    private static $plural_name = 'AdsCosts';

    private static $table_name = 'HestecLinkManagerAdsCost';

    private static $db = array(
        'MonthDate' => 'Date',
        'GoogleAds' => 'Currency',
        'BingAds' => 'Currency'
    );

    private static $default_sort='MonthDate DESC';

    private static $summary_fields = array(
        'getMonthYear',
        'GoogleAds',
        'BingAds'
    );

    public function getCMSFields() {

        $MonthDateSource = array();
        $m = 1;
        while ($m < 13){

            $now = new \DateTime();
            $now->modify('-'.$m.' months');

            if (AdsCost::get()->filter('MonthDate', $now->format('Y-m-t'))->count() == 0){

                $MonthDateSource[$now->format('Y-m-t')] = $now->format('F Y');

            }

            $m++;

        }

        if ($this->ID){

            $MonthDateField = ReadonlyField::create('MonthDateInfo', 'Month', $this->getMonthYear());

        }else{

            $MonthDateField = DropdownField::create('MonthDate', 'Month', $MonthDateSource);

        }

        $GoogleAdsField = CurrencyField::create('GoogleAds', "GoogleAds");
        $BingAdsField = CurrencyField::create('BingAds', "BingAds");

        return new FieldList(
            $MonthDateField,
            $GoogleAdsField,
            $BingAdsField
        );

    }

    public function getMonthYear(){

        $d = new \DateTime($this->MonthDate);
        return DBField::create_field('HTMLText', $d->format('F Y'));

    }

}