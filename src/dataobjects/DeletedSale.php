<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;

class DeletedSale extends DataObject {

    private static $table_name = 'HestecLinkManagerDeletedSale';

    private static $db = [
        'TransactionId' => 'Varchar(255)',
        'AffiliateNetwork' => "Enum('Daisycon,Awin,TradeTracker,other','')"
    ];

}
