<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\FieldList;

class Visitor extends DataObject {

    private static $table_name = 'HestecLinkManagerVisitor';

    private static $db = [
        'Ip' => 'Varchar(50)',
        'IpMd5' => 'Varchar(40)',
        'Entry' => 'Varchar(255)',
        'Referer' => 'Varchar(255)',
        'Source' => "Enum('GoogleAds,BingAds','')",
        'SourceClickId' => 'Varchar(255)',
        'Device' => 'Varchar(20)',
        'Browser' => 'Varchar(40)',
        'QueryString' => 'Varchar(255)',
        'HttpUserAgent' => 'Varchar(255)',
        'BotDetected' => 'Boolean'
    ];

    private static $default_sort='ID DESC';

    private static $has_many = array(
        'Sales' => Sale::class,
        'Clicks' => Click::class
    );

    private static $summary_fields = array(
        'ID' => 'ID',
        'Created' => 'Date',
        'Ip' => 'IP',
        'ParsedReferer' => 'Referer',
        'Entry' => 'Entry',
        'Source' => 'Source',
        'Device' => 'Device',
        'Browser' => 'Browser',
        'Clicks.Count' => 'Clicks',
        'Sales.Count' => 'Sales'
    );

    public function getCMSFields() {

        $ClicksGridField = GridField::create(
            'Clicks',
            'Clicks',
            $this->Clicks(),
            GridFieldConfig_RecordEditor::create()
        //->addComponent(new GridFieldOrderableRows())
        );

        $IDField = NumericField::create('ID', "ID");
        $IDField->setReadonly(true);


        return new FieldList(
            $IDField,
            $ClicksGridField
        );

    }

    public function ParsedReferer(){

        $output = parse_url($this->Referer, PHP_URL_HOST);

        return DBField::create_field('HTMLText', $output);

    }

}