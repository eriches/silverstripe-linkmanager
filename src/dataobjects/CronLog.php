<?php

namespace Hestec\LinkManager;

use SilverStripe\ORM\DataObject;

class CronLog extends DataObject {

    private static $table_name = 'HestecLinkManagerCronLog';

    private static $db = [
        'LastRun' => 'Datetime'
    ];

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (CronLog::get()->count() == 0){

            $runtime = new \DateTime();
            $record = new CronLog();
            $record->LastRun = $runtime->format('Y-m-d');
            $record->write();

        }

    }

}