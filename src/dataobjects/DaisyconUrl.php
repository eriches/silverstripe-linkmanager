<?php

namespace Hestec\LinkManager;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

class DaisyconUrl extends DataObject {

    private static $table_name = 'HestecLinkManagerDaisyconUrl';

    private static $db = [
        'Domain' => 'Varchar(255)'
    ];

    private static $summary_fields = array(
        'Domain' => 'Domain'
    );

    public function getCMSFields() {

        $DomainField = TextField::create('Domain', 'Domain');
        $DomainField->setDescription("Bijvoorbeeld: lt45.net");

        return new FieldList(
            $DomainField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'Domain'
        ));
    }

}