<?php

namespace Hestec\LinkManager;

use SilverStripe\Dev\CsvBulkLoader;

class TlgCsvBulkLoader extends CsvBulkLoader
{
    public $columnMap = [
        'ID' => 'TransactionId',
        'Commission' => 'Commission',
        'Order ID' => 'OrderId',
        'Product ID' => 'ProductId',
        'Created' => 'SaleDate',
        'Campaign Name' => 'CampaignName',
        'commissionTypeName' => 'CommissionTypeName',
        'Status' => 'Status',
        'First Click Time' => 'FirstClickTime',
        'Last Click Time' => 'LastClickTime'
    ];
}