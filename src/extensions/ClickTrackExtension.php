<?php

namespace Hestec\LinkManager;

use SilverStripe\Core\Extension;
use SilverStripe\Core\Config\Config;
use DeviceDetector\DeviceDetector;

class ClickTrackExtension extends Extension
{

    public function onAfterInit(){

        $dd = new DeviceDetector($_SERVER['HTTP_USER_AGENT']);
        $dd->discardBotInformation();
        $dd->parse();

        $extra_bot_check = false;
        if ($dd->isBot()){
            $extra_bot_check = true;
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Google-Read-Aloud') !== false){
                $extra_bot_check = false;
            }
        }

        if (!$this->owner->getRequest()->getSession()->get('affvisitor') && $extra_bot_check === false){

            $track = new Visitor();
            $track->Ip = $_SERVER['REMOTE_ADDR'];
            $track->IpMd5 = md5($_SERVER['REMOTE_ADDR']);
            $track->Entry = strtok($_SERVER['REQUEST_URI'], '?');
            if (isset($_SERVER['HTTP_REFERER'])){
                $track->Referer = $_SERVER['HTTP_REFERER'];
            }
            $track->QueryString = $_SERVER['QUERY_STRING'];
            $track->HttpUserAgent = $_SERVER['HTTP_USER_AGENT'];
            if (isset($_GET['gclid'])){
                $track->Source = 'GoogleAds';
                $track->SourceClickId = $_GET['gclid'];
            }
            if (isset($_GET['msclkid'])){
                $track->Source = 'BingAds';
                $track->SourceClickId = $_GET['msclkid'];
            }
            $track->Device = $dd->getDeviceName();
            $track->Browser = $dd->getClient('name')." ".$dd->getClient('version');
            if ($dd->isBot()){
                $track->BotDetected = true;
            }
            $track->write();

            $this->owner->getRequest()->getSession()->set('affvisitor', $track->ID);

        }

    }

    public function TrackingId($widget = null)
    {

        $site = 'x';
        if (Config::inst()->get('AffiliateDetails', 'AffiliateSite')){
            $site = Config::inst()->get('AffiliateDetails', 'AffiliateSite');
        }
        $visitor = 'BOT';
        if ($this->owner->getRequest()->getSession()->get('affvisitor')){
            $visitor = $this->owner->getRequest()->getSession()->get('affvisitor');
        }
        $widgetid = 'x';
        if (isset($widget)){
            $widgetid = $widget;
        }

        return "_qca".$site."_".$visitor."_x_".$widgetid."_".$this->owner->ID."h_";

    }

    public function AbTest()
    {

        if ($this->owner->getRequest()->getSession()->get('affvisitor') % 2 == 0){
            return "A"; // even
        }
        return "B"; //odd

    }

}
