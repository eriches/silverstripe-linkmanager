<?php

use SilverStripe\Control\Controller;
use SilverStripe\Core\Config\Config;
use Hestec\LinkManager\Click;
use Hestec\LinkManager\Visitor;
use SilverStripe\Core\Environment;
use SilverStripe\Control\HTTPResponse;

class link extends Controller {

    private static $allowed_actions = array (
        'id'
    );

    public function id(){

        $Params = $this->getURLParams();

        if ($Params['Action'] == "id" && is_numeric($Params['ID'])) {

            if ($output = \Hestec\LinkManager\Link::get()->byID($Params['ID'])) {

                if ($this->getRequest()->getSession()->get('affvisitor') && $refpage = $this->getRefPage()) {

                    $click = new Click();
                    if (isset($refpage)){
                        $click->RefPage = $refpage;
                    }
                    $click->Action = "Click: ".$output->Title;
                    $click->LinkID = $output->ID;
                    $click->VisitorID = $this->getRequest()->getSession()->get('affvisitor');
                    $click->write();

                }else{

                    $click = new Click();
                    $click->Ip = $_SERVER['REMOTE_ADDR'];
                    $click->HttpUserAgent = $_SERVER['HTTP_USER_AGENT'];
                    if (isset($_SERVER['HTTP_REFERER'])){
                        $click->Referer = $_SERVER['HTTP_REFERER'];
                    }
                    $click->Action = "Click: ".$output->Title;
                    $click->LinkID = $output->ID;

                    if ($visitorbyip = Visitor::get()->filter('Ip', $_SERVER['REMOTE_ADDR'])->first()){

                        $click->VisitorID = $visitorbyip->ID;

                    }

                    $click->write();

                }

                if ($output->AffiliateNetwork && $output->AffiliateNetwork !== 'other'){

                    $domain = str_replace('www.', '', $_SERVER['HTTP_HOST']);

                    switch($output->AffiliateNetwork){
                        case "Daisycon":
                            $refparam = "&ws=";
                            break;
                        case "Awin":
                            $refparam = "&clickref=".$domain."&clickref2=";
                            break;
                        case "Bol":
                            $refparam = "&subid=";
                            break;
                        case "Tradedoubler":
                            $refparam = "&epi=";
                            break;
                        case "Adtraction":
                            $refparam = "&epi=";
                            break;
                        case "Booking":
                            $refparam = "&label=";
                            break;
                        case "Whitelabeled":
                            $refparam = "&t=";
                            break;
                    }
                    $site = 'x';
                    if (Config::inst()->get('AffiliateDetails', 'AffiliateSite')){
                        $site = Config::inst()->get('AffiliateDetails', 'AffiliateSite');
                    }
                    $visitor = 'BOT';
                    if ($this->getRequest()->getSession()->get('affvisitor')){
                        $visitor = $this->getRequest()->getSession()->get('affvisitor');
                    }elseif (isset($visitorbyip->ID)){
                        $visitor = $visitorbyip->ID;
                    }
                    $clickid = 'x';
                    if (isset($click->ID)){
                        $clickid = $click->ID;
                    }
                    $refvalue = '_qca'.$site.'_'.$visitor.'_'.$clickid.'h_';

                    if ($output->AffiliateNetwork == "TradeTracker" || $output->AffiliateNetwork == "Coolblue" || $output->AffiliateNetwork == "CJ" || $output->AffiliateNetwork == "Partnerize"){

                        return $this->redirect(str_replace('TTSUBIDHESTEC', str_replace('_', '-', $refvalue), $output->Url));

                    }

                    return $this->redirect($output->Url.$refparam.$refvalue);

                }else{

                    return $this->redirect($output->Url);

                }


            }else{

                return "link not exists";

            }

        }else{

            return "invalid call";

        }

    }

    public function getRefPage(){

        if (isset($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == parse_url(Environment::getEnv('SS_CANONICAL_BASE_URL'), PHP_URL_HOST)){

            $ref = strstr(strtok($_SERVER['HTTP_REFERER'], '?'), $_SERVER['HTTP_HOST']); //gets all text from needle on
            $ref = str_replace($_SERVER['HTTP_HOST'], '', $ref);

            return $ref;

        }
        return false;

    }

}
