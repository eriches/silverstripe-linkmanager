<?php

namespace Hestec\LinkManager;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldPrintButton;
use SilverStripe\Forms\GridField\GridFieldImportButton;
use SilverStripe\Forms\GridField\GridFieldExportButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class LinkManagerAdmin extends ModelAdmin {

    private static $managed_models = array(
        Sale::class,
        Visitor::class,
        Click::class,
        Link::class,
        AdsCost::class,
        TlgSale::class,
        TlgType::class,
        DaisyconUrl::class
    );

    // disable the importer
    private static $model_importers = array(
        TlgSale::class => TlgCsvBulkLoader::class
    );

    // Linked as /admin/slides/
    private static $url_segment = 'linkmanager';

    // title in cms navigation
    private static $menu_title = 'LinkManager';

    // menu icon
    //private static $menu_icon = 'resources/hestec/silverstripe-exactonline/client/images/icons/icon-exactonline.png';

    public function getList() {
        $list = parent::getList();

        $createddate = new \DateTime();
        $createddate->modify('-3 days');

        if($this->modelClass == Visitor::class) {
            $list = $list->filter(array('Clicks.Count():GreaterThan' => 0, 'Created:GreaterThan' => $createddate->format('Y-m-d')));
        }
        if($this->modelClass == Click::class) {
            $list = $list->sort('ClickDate DESC');
        }

        return $list;
    }

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);

        // get gridfield
        $gridfield = $form->Fields()
            ->dataFieldByName($this->sanitiseClassName($this->modelClass));

        $gridfieldConfig = $gridfield->getConfig();

        //if($this->modelClass == Sale::class || $this->modelClass == Visitor::class || $this->modelClass == Click::class) {
        if($this->modelClass == Visitor::class || $this->modelClass == Click::class) {
            $gridfieldConfig->removeComponentsByType(GridFieldDeleteAction::class);
            $gridfieldConfig->removeComponentsByType(GridFieldAddNewButton::class);
            $gridfieldConfig->removeComponentsByType(GridFieldPrintButton::class);
            $gridfieldConfig->removeComponentsByType(GridFieldImportButton::class);
            $gridfieldConfig->removeComponentsByType(GridFieldExportButton::class);
        }

        if($this->modelClass == TlgSale::class) {
            $gridfieldConfig->removeComponentsByType(GridFieldDeleteAction::class);
            $gridfieldConfig->removeComponentsByType(GridFieldAddNewButton::class);
            $gridfieldConfig->removeComponentsByType(GridFieldPrintButton::class);
            //$gridfieldConfig->removeComponentsByType(GridFieldImportButton::class);
            $gridfieldConfig->removeComponentsByType(GridFieldExportButton::class);
        }

        if($this->modelClass == TlgType::class) {
            $gridfieldConfig->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }

}